package Ejercicio.ToleGroup.BackendGameofthrones.controlador;

import Ejercicio.ToleGroup.BackendGameofthrones.servicio.UsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UsuarioControlador {

    @Autowired
    UsuarioServicio usuarioServicio;


    @GetMapping("/Iniciarsesion")
    public boolean iniciarsesion(){
        return true;
    }

    @PostMapping("/crearusuario")
    public boolean crearusuario(){
        return true;
    }


}
