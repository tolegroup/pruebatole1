package Ejercicio.ToleGroup.BackendGameofthrones.repositorio;

import Ejercicio.ToleGroup.BackendGameofthrones.modelo.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario,Integer> {
}
