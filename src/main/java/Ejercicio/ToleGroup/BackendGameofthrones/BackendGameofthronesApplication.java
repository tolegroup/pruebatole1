package Ejercicio.ToleGroup.BackendGameofthrones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendGameofthronesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendGameofthronesApplication.class, args);
	}

}
